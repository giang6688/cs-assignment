Credit Suisse assignment - Drawing console
========================

##### How to run:
- 1st time (build & execute):  
`mvn clean package exec:java`

- 2nd time:  
`mvn exec:java`  
alternatively  
`java -jar target\drawing-console-1.0-jar-with-dependencies.jar`

##### Assumption & limitation:
- Assume all requests which go beyond Screen boundary will be cancelled & exception will be thrown
- Only able to draw either vertical or horizontal line at moment
- Command is case in-sensitive
- If Screen size given is big, need to increase default stack size using `-Xss` to overcome StackOverflowError
<br>Sample: `java -jar -Xss2048k target\drawing-console-1.0-jar-with-dependencies.jar`

###### developed by: 
Ethan Nguyen Hoang Giang  
[giangnh66@gmail.com](mailto:giangnh66@gmail.com)  
+65.9387.9368