package com.cs.assignment.drawing.instruction;

import lombok.Data;

@Data
public class CreateScreenInstruction implements Instruction {
    private Integer x;

    private Integer y;

    @Override
    public CreateScreenInstruction parseCommand(String cmd) {
        String[] tokenizedInput = cmd.trim().split(SEPARATOR);

        x = Instruction.parseInt(tokenizedInput[1]);

        y = Instruction.parseInt(tokenizedInput[2]);

        return this;
    }
}
