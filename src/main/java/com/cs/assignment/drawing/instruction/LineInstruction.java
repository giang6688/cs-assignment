package com.cs.assignment.drawing.instruction;

import lombok.Data;

@Data
public class LineInstruction implements Instruction {
    private Integer x1;

    private Integer y1;

    private Integer x2;

    private Integer y2;

    public Boolean isHorizontal() {
        return y1.equals(y2);
    }

    public Boolean isVertical() {
        return x1.equals(x2);
    }

    @Override
    public LineInstruction parseCommand(String cmd) {
        String[] tokenizedInput = cmd.trim().split(SEPARATOR);

        x1 = Instruction.parseInt(tokenizedInput[1]);

        y1 = Instruction.parseInt(tokenizedInput[2]);

        x2 = Instruction.parseInt(tokenizedInput[3]);

        y2 = Instruction.parseInt(tokenizedInput[4]);

        return this;
    }
}
