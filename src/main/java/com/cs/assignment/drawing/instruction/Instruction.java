package com.cs.assignment.drawing.instruction;

public interface Instruction {
    String SEPARATOR = " ";

    Instruction parseCommand(String cmd);

    static Integer parseInt(String value) {
        try {
            return Integer.valueOf(value);
        } catch (Exception e) {
            return null;
        }
    }
}
