package com.cs.assignment.drawing.instruction;

import com.cs.assignment.drawing.command.Line;
import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class RectangleInstruction implements Instruction {
    private int x1;

    private int y1;

    private int x2;

    private int y2;

    private List<String> lineCommands = new ArrayList<>();

    @Override
    public RectangleInstruction parseCommand(String cmd) {
        String[] tokenizedInput = cmd.trim().split(SEPARATOR);

        x1 = Instruction.parseInt(tokenizedInput[1]);

        y1 = Instruction.parseInt(tokenizedInput[2]);

        x2 = Instruction.parseInt(tokenizedInput[3]);

        y2 = Instruction.parseInt(tokenizedInput[4]);

        return this;
    }

    /**
     * Translate the instruction to multiple lines instead
     *
     * @return List <{@link Line}>
     */
    public List<String> translateToLineCommands() {
        // Instruction 2 horizontal lines
        lineCommands.add("L " + x1 + " " + y1 + " " + x2 + " " + y1);
        lineCommands.add("L " + x1 + " " + y2 + " " + x2 + " " + y2);

        // Instruction 2 vertical lines
        lineCommands.add("L " + x1 + " " + y1 + " " + x1 + " " + y2);
        lineCommands.add("L " + x2 + " " + y1 + " " + x2 + " " + y2);

        return lineCommands;
    }
}
