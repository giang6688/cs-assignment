package com.cs.assignment.drawing.instruction;

import lombok.Data;

@Data
public class BucketFillInstruction implements Instruction {
    private int x;

    private int y;

    private String color;

    @Override
    public BucketFillInstruction parseCommand(String cmd) {
        String[] tokenizedInput = cmd.trim().split(SEPARATOR);

        x = Instruction.parseInt(tokenizedInput[1]);

        y = Instruction.parseInt(tokenizedInput[2]);

        color = tokenizedInput[3];

        return this;
    }
}
