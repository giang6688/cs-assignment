package com.cs.assignment.drawing.constant;

/**
 * To organize commands in expected order in chain
 */
public interface CommandOrder {
    Integer MENU = Integer.MIN_VALUE;

    Integer SCREEN_INIT = 1;

    Integer DRAWING = 2;

    Integer BASE_DRAWING = 3;

    Integer NOT_SUPPORTED = Integer.MAX_VALUE;
}
