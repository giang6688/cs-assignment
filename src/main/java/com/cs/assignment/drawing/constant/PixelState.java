package com.cs.assignment.drawing.constant;

public interface PixelState {
    String DISABLED = " ";

    String ENABLED = "x";

    String DEFAULT_COLOR = "o";

    String VISITED = "V";
}
