package com.cs.assignment.drawing;

import com.cs.assignment.drawing.model.Monitor;

import java.util.Scanner;

public class DrawingConsoleApplication {

    public static void main(String[] args) {
        Monitor monitor = new Monitor();
        monitor.init();

        while (true) {
            try {
                System.out.print("enter command: ");
                Scanner scanner = new Scanner(System.in);

                monitor.validateAndExecuteInstruction(scanner.nextLine());

                System.out.println();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

}
