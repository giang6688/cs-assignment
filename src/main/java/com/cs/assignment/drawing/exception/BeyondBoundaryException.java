package com.cs.assignment.drawing.exception;

public class BeyondBoundaryException extends Exception {
    public BeyondBoundaryException(String message) {
        super(message);
    }
}
