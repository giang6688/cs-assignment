package com.cs.assignment.drawing.model;

import com.cs.assignment.drawing.constant.PixelState;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@EqualsAndHashCode
public class Pixel {

    private String value = PixelState.DISABLED;

    @NonNull private int x;

    @NonNull private int y;

    private String tmpState;

    /**
     * Turn ON the pixel
     * <br>by setting its value to {@link PixelState#ENABLED}
     * @return {@link Pixel}
     */
    public Pixel on() {
        value = PixelState.ENABLED;
        return this;
    }

    /**
     * Turn ON the pixel
     * <br>by setting its value to {@link PixelState#DISABLED}
     * @return {@link Pixel}
     */
    public Pixel off() {
        value = PixelState.DISABLED;
        return this;
    }

    /**
     * Turn ON the pixel
     * <br>by setting its value to given color
     * @return {@link Pixel}
     */
    public Pixel fill(String color) {
        value = color;
        return this;
    }

    @Override
    public String toString() {
        return new StringBuilder()
                //.append("(")
                .append(x)
                .append(":")
                .append(y)
//                .append(")")
                .append("[")
//                .append("_")
                .append(value)
                .append("]").append(" ")
                .toString();
    }

}
