package com.cs.assignment.drawing.model;

import com.cs.assignment.drawing.command.Command;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.reflections.Reflections;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Data
@Slf4j
public class Monitor {
    private Screen screen;

    private List<Command> supportedCommands = new ArrayList();

    /**
     * Create processing chain
     */
    public void init() {

        Reflections reflections = new Reflections("com.cs.assignment.drawing.command");
        Set<Class<? extends Command>> commands = reflections.getSubTypesOf(Command.class);


        for (Class<? extends Command> clazz : commands) {
            try {
                supportedCommands.add(clazz.newInstance());
            } catch (InstantiationException e) {
                e.printStackTrace();
            } catch (IllegalAccessException e) {
                e.printStackTrace();
            }
        }

        Collections.sort(supportedCommands);
        for (int i = 0; i < supportedCommands.size() - 1; i++) {
            supportedCommands.get(i).setNext(supportedCommands.get(i+1));
        }

        if (log.isTraceEnabled()) {
            supportedCommands.forEach(c -> log.trace("{} => {}", c, c.getNext()));
        }
    }

    public void validateAndExecuteInstruction(String input) throws Exception {
        supportedCommands.get(0).validateAndExecuteCommand(input, this);

        refresh();
    }

    public void refresh() {
        if (screen != null) screen.refresh();
    }
}