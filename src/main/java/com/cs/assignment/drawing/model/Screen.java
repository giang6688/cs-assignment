package com.cs.assignment.drawing.model;

import com.cs.assignment.drawing.constant.PixelState;
import com.cs.assignment.drawing.exception.BeyondBoundaryException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Screen {
    private int width;

    private int height;

    private String header;

    private String footer;

    private String sideBorder = "|";

    private List<Pixel> pixels = new ArrayList<>();

    public Screen(int width, int height) {
        this.width = width;

        this.height = height;

        for (int y = 1; y <= height; y++) {
            for (int x = 1; x <= width; x++) {
                pixels.add(new Pixel(x, y));
            }
        }

        // ScreenInit header
        StringBuilder sb = new StringBuilder();
        for (int x=0; x <= width + 1; x++) {
            sb.append("-");
        }
        header = sb.toString();
        footer = sb.toString();

    }

    /**
     * Get list of {@link Pixel} which value has been set to {@link PixelState#ENABLED}
     * @return List <{@link Pixel}
     */
    public List<Pixel> getListOfPixelsTurnedOn() {
        return pixels.parallelStream()
                .filter(p -> p.getValue().equals(PixelState.ENABLED))
                .collect(Collectors.toList());
    }

    /**
     * Get list of {@link Pixel} which value has been set any color
     * @return List <{@link Pixel}
     */
    public List<Pixel> getListOfPixelsColored() {
        return pixels.parallelStream()
                .filter(p -> ! (p.getValue().equals(PixelState.DISABLED) || p.getValue().equals(PixelState.ENABLED)) )
                .collect(Collectors.toList());
    }

    /**
     * Get {@link Pixel} by given coordinate
     *
     * @param x axis
     * @param y axis
     * @return {@link Pixel}
     * @throws BeyondBoundaryException
     */
    public Pixel getByCoordinate(int x, int y) throws BeyondBoundaryException {
        if (x <= 0 || y <= 0 || x > width || y > height) {
            throw new BeyondBoundaryException("wrong coordinate given [" + x + ":" + y +"]");
        }
        return pixels.get((y-1) * width + x - 1);
    }

    public void refresh() {
        refresh(false);
    }

    public void refresh(boolean debugMode) {
        int c = 0;
        System.out.println(getHeader());
        for (Pixel pixel : getPixels()) {
            if (c == 0) {
                System.out.print("|");
            }
            System.out.print(debugMode ? pixel.toString() : pixel.getValue());
            c++;
            if (c == getWidth()) {
                System.out.println("|");
                c = 0;
            }
        }
        System.out.println(getFooter());
    }
}