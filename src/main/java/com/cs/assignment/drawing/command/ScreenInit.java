package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.constant.CommandOrder;
import com.cs.assignment.drawing.exception.InvalidCommandException;
import com.cs.assignment.drawing.instruction.Instruction;
import com.cs.assignment.drawing.instruction.CreateScreenInstruction;
import com.cs.assignment.drawing.model.Monitor;
import com.cs.assignment.drawing.model.Screen;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.regex.Pattern;

@Slf4j
@Data
public class ScreenInit extends Command {

    @Override
    protected Pattern getCommandPattern() {
        return Pattern.compile("^[cC]\\s\\d+\\s\\d+$");
    }

    @Override
    public boolean validateAndExecuteCommand(String cmd, Monitor monitor) throws Exception {
        if (super.isValidCommand(cmd)) {
            CreateScreenInstruction detail = new CreateScreenInstruction().parseCommand(cmd);
            executeCommand(detail, monitor);

            return true;
        } else {
            if (monitor.getScreen() == null) throw new InvalidCommandException("Please initialize Screen first!");

            return executeNextCommand(cmd, monitor);
        }
    }

    @Override
    protected void executeCommand(Instruction inputCmd, Monitor monitor) {
        log.trace("Execute::{}", this.getClass().getName());
        CreateScreenInstruction cmd = (CreateScreenInstruction) inputCmd;
        monitor.setScreen(new Screen(cmd.getX(), cmd.getY()));
        log.trace("Created [{}]", monitor.getScreen());
    }

    @Override
    public Integer order() {
        return CommandOrder.SCREEN_INIT;
    }
}