package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.constant.PixelState;
import com.cs.assignment.drawing.exception.BeyondBoundaryException;
import com.cs.assignment.drawing.exception.InvalidCommandException;
import com.cs.assignment.drawing.instruction.BucketFillInstruction;
import com.cs.assignment.drawing.instruction.Instruction;
import com.cs.assignment.drawing.model.Monitor;
import com.cs.assignment.drawing.model.Pixel;
import com.cs.assignment.drawing.model.Screen;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.regex.Pattern;

@Slf4j
@Data
public class BucketFill extends Command {

    @Override
    protected Pattern getCommandPattern() {
        return Pattern.compile("^[bB]\\s\\d+\\s\\d+\\s.*$");
    }

    /**
     * validate the given coordinates whether X & Y axis are valid int number
     * <br/>also validating color option
     *
     * @return TRUE or FALSE
     * @throws Exception
     */
    @Override
    public boolean validateAndExecuteCommand(String cmd, Monitor monitor) throws Exception {
        if (super.isValidCommand(cmd)) {
            BucketFillInstruction detail = new BucketFillInstruction().parseCommand(cmd);

            if (detail.getX() > monitor.getScreen().getWidth() || detail.getY() > monitor.getScreen().getHeight()) {
                throw new BeyondBoundaryException("beyond Screen boundary");
            }

            if (detail.getColor() == null || detail.getColor().isEmpty() || PixelState.ENABLED.equals(detail.getColor())) {
                throw new InvalidCommandException("Please pick another color");
            }

            executeCommand(detail, monitor);

            return true;
        } else {
            return executeNextCommand(cmd, monitor);
        }
    }

    @Override
    protected void executeCommand(Instruction inputCmd, Monitor monitor) throws Exception {
        log.trace("Execute::{}", this.getClass().getName());

        BucketFillInstruction cmd = (BucketFillInstruction) inputCmd;
        Screen screen = monitor.getScreen();

        markConnectedPixels(cmd.getX(), cmd.getY(), screen);

        screen.getPixels().parallelStream()
                .filter(p -> PixelState.VISITED.equals(p.getTmpState()))
                .forEach(p -> {
//                    System.out.println("connected:" + p);
                    p.setTmpState(null);
                    p.fill(cmd.getColor());
                });
    }

    /**
     * Get all Pixels connected to the Pixel with given coordinate
     *
     * @param x X coordinator
     * @param y Y coordinator
     */
    private void markConnectedPixels(int x, int y, Screen screen) throws BeyondBoundaryException {
        if (x > screen.getWidth() || y > screen.getHeight())
            return;

        if (x <= 0 || y <= 0)
            return;

        Pixel pixel = screen.getByCoordinate(x, y);
        // Avoid visiting same node again
        if (PixelState.VISITED.equals(pixel.getTmpState())) {
            return;
        }
        // Ignore lines
        else if (PixelState.ENABLED.equals(pixel.getValue())) {
            return;
        }

        pixel.setTmpState(PixelState.VISITED);

        markConnectedPixels(x - 1, y, screen);
        markConnectedPixels(x, y - 1, screen);
        markConnectedPixels(x, y + 1, screen);
        markConnectedPixels(x + 1, y, screen);
    }
}