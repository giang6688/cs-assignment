package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.constant.CommandOrder;
import com.cs.assignment.drawing.instruction.Instruction;
import com.cs.assignment.drawing.model.Monitor;
import lombok.Data;

import java.util.regex.Pattern;

@Data
public abstract class Command implements Comparable<Command> {
    protected final static String SEPARATOR = " ";

    protected Command next;

    protected abstract Pattern getCommandPattern();

    public abstract boolean validateAndExecuteCommand(String cmd, Monitor monitor) throws Exception;

    boolean isValidCommand(String input) {
        return getCommandPattern().matcher(input.trim()).matches();
    }

    protected abstract void executeCommand(Instruction cmd, Monitor monitor) throws Exception;

    boolean executeNextCommand(String cmd, Monitor monitor) throws Exception {
        return next != null && next.validateAndExecuteCommand(cmd, monitor);
    }

    public Integer order() {
        return CommandOrder.DRAWING;
    }

    @Override
    public int compareTo(Command o) {
        if (this == o) return 0;
        return this.order().compareTo(o.order());
    }

}