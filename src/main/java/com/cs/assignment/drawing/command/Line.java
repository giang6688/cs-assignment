package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.constant.CommandOrder;
import com.cs.assignment.drawing.exception.BeyondBoundaryException;
import com.cs.assignment.drawing.exception.InvalidCommandException;
import com.cs.assignment.drawing.instruction.Instruction;
import com.cs.assignment.drawing.instruction.LineInstruction;
import com.cs.assignment.drawing.model.Monitor;
import com.cs.assignment.drawing.model.Screen;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.regex.Pattern;

@Slf4j
@Data
public class Line extends Command {

    @Override
    protected Pattern getCommandPattern() {
        return Pattern.compile("^[lL]\\s\\d+\\s\\d+\\s\\d+\\s\\d+$");
    }

    /**
     * validate the given coordinates whether X & Y axis are valid int number
     * <br/>also validating if line is horizontal or vertical
     *
     * @return TRUE or FALSE
     * @throws Exception
     */
    @Override
    public boolean validateAndExecuteCommand(String cmd, Monitor monitor) throws Exception {
        if (super.isValidCommand(cmd)) {
            Screen screen = monitor.getScreen();
            LineInstruction detail = new LineInstruction().parseCommand(cmd);

            if (!detail.isHorizontal() && !detail.isVertical()) {
                throw new InvalidCommandException("Only support either vertical or horizontal line. Input command was [" + cmd + "]");
            }

            if (detail.getX1() > screen.getWidth() || detail.getX2() > screen.getWidth() || detail.getY1() > screen.getHeight() || detail.getY2() > screen.getHeight()) {
                throw new BeyondBoundaryException("beyond Screen boundary. Input command was [" + cmd + "]");
            }

            executeCommand(detail, monitor);

            return true;
        } else {
            return executeNextCommand(cmd, monitor);
        }
    }

    @Override
    protected void executeCommand(Instruction inputCmd, Monitor monitor) throws BeyondBoundaryException {
        log.trace("Execute::{}", this.getClass().getName());

        LineInstruction cmd = (LineInstruction) inputCmd;
        Screen screen = monitor.getScreen();

        // Change pixels belong to this line
        if (cmd.isHorizontal()) {
            for (int i = cmd.getX1(); i <= cmd.getX2(); i++) {
                screen.getByCoordinate(i, cmd.getY1()).on();
            }
        } else {
            for (int i = cmd.getY1(); i <= cmd.getY2(); i++) {
                screen.getByCoordinate(cmd.getX1(), i).on();
            }
        }
    }

    @Override
    public Integer order() {
        return CommandOrder.BASE_DRAWING;
    }
}