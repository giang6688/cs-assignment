package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.constant.CommandOrder;
import com.cs.assignment.drawing.exception.InvalidCommandException;
import com.cs.assignment.drawing.instruction.Instruction;
import com.cs.assignment.drawing.model.Monitor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.regex.Pattern;

@Slf4j
@Data
public class NotSupported extends Command {

    @Override
    protected Pattern getCommandPattern() {
        return Pattern.compile("^.*$");
    }

    @Override
    public boolean validateAndExecuteCommand(String cmd, Monitor monitor) throws InvalidCommandException {
        throw new InvalidCommandException("Not supported command [" + cmd + "]");
    }

    @Override
    public void executeCommand(Instruction cmd, Monitor monitor) {
        // Do nothing
    }

    @Override
    public Integer order() {
        return CommandOrder.NOT_SUPPORTED;
    }
}