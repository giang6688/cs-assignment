package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.constant.CommandOrder;
import com.cs.assignment.drawing.instruction.Instruction;
import com.cs.assignment.drawing.model.Monitor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.regex.Pattern;

@Slf4j
@Data
public class Exit extends Command {

    @Override
    protected Pattern getCommandPattern() {
        return Pattern.compile("^[qQ]$");
    }

    @Override
    public boolean validateAndExecuteCommand(String cmd, Monitor monitor) throws Exception {
        if (super.isValidCommand(cmd)) {
            executeCommand(null, monitor);
            return true;
        } else {
            return executeNextCommand(cmd, monitor);
        }
    }

    @Override
    protected void executeCommand(Instruction cmd, Monitor monitor) {
        System.exit(0);
    }

    @Override
    public Integer order() {
        return CommandOrder.MENU;
    }
}