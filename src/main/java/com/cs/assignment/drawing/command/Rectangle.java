package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.exception.InvalidCommandException;
import com.cs.assignment.drawing.instruction.Instruction;
import com.cs.assignment.drawing.instruction.RectangleInstruction;
import com.cs.assignment.drawing.model.Monitor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.util.regex.Pattern;

@Slf4j
@Data
public class Rectangle extends Command {

    @Override
    protected Pattern getCommandPattern() {
        return Pattern.compile("^[rR]\\s\\d+\\s\\d+\\s\\d+\\s\\d+");
    }

    /**
     * validate the given coordinates whether X & Y axis are valid int number
     * <br>also expecting a "space" between 2 horizontal line & 2 vertical lines
     *
     * @return TRUE or FALSE
     * @throws Exception
     */
    @Override
    public boolean validateAndExecuteCommand(String cmd, Monitor monitor) throws Exception {
        if (super.isValidCommand(cmd)) {
            RectangleInstruction detail = new RectangleInstruction().parseCommand(cmd);

            // Expect there is a "space" between 2 horizontal line & 2 vertical lines
            if (detail.getX2() - detail.getX1() <= 1) {
                throw new InvalidCommandException("Not supported rectangle");
            }

            if (detail.getY2() - detail.getY1() <= 1) {
                throw new InvalidCommandException("Not supported rectangle");
            }

            executeCommand(detail, monitor);

            return true;
        } else {
            return executeNextCommand(cmd, monitor);
        }
    }

    @Override
    protected void executeCommand(Instruction inputCmd, Monitor monitor) throws Exception {
        log.trace("Execute::{}", this.getClass().getName());

        RectangleInstruction cmd = (RectangleInstruction) inputCmd;

        for (String command : cmd.translateToLineCommands()) {
            this.validateAndExecuteCommand(command, monitor);
        }
    }
}