package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.model.Monitor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

public class ExitCommandTest {
    private Monitor monitor = new Monitor();

    @Mock
    private ScreenInit nextCommand;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);

    }

    @Test
    public void invokeChain_success() throws Exception {
        Exit exit = new Exit();
        exit.setNext(nextCommand);

        exit.validateAndExecuteCommand("C 10 4", monitor);

        Mockito.verify(nextCommand).validateAndExecuteCommand("C 10 4", monitor);
    }

}
