package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.exception.InvalidCommandException;
import com.cs.assignment.drawing.instruction.RectangleInstruction;
import com.cs.assignment.drawing.model.Monitor;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import static org.mockito.Mockito.*;

public class RectangleTest {
    private Monitor monitor = new Monitor();

    private ScreenInit screenInit = new ScreenInit();

    @Mock
    Line line;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        screenInit.validateAndExecuteCommand("C 20 4", monitor);
    }

    @Test(expected = InvalidCommandException.class)
    public void invalidRectangle1_expectInvalidCommandException() throws Exception {
        Rectangle rectangle = new Rectangle();
        rectangle.validateAndExecuteCommand("R 1 2 10 2", monitor);
    }

    @Test(expected = InvalidCommandException.class)
    public void invalidRectangle2_expectInvalidCommandException() throws Exception {
        Rectangle rectangle = new Rectangle();
        rectangle.validateAndExecuteCommand("R 1 2 2 2", monitor);
    }

    @Test
    public void validRectangle_expectChainProcessing() throws Exception {
        String strCmd = "R 14 1 18 3";

        Rectangle rectangle = new Rectangle();
        rectangle.setNext(line);

        rectangle.validateAndExecuteCommand(strCmd, monitor);

        verify(line, times(4)).validateAndExecuteCommand(anyString(), any(Monitor.class));

        RectangleInstruction detail = new RectangleInstruction().parseCommand(strCmd);
        detail.translateToLineCommands();
        Assert.assertTrue(detail.getLineCommands().contains("L 14 1 18 1"));
        Assert.assertTrue(detail.getLineCommands().contains("L 14 1 14 3"));
        Assert.assertTrue(detail.getLineCommands().contains("L 14 3 18 3"));
        Assert.assertTrue(detail.getLineCommands().contains("L 18 1 18 3"));
    }
}
