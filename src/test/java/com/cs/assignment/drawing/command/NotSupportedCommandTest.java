package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.exception.InvalidCommandException;
import com.cs.assignment.drawing.model.Monitor;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

public class NotSupportedCommandTest {
    @Spy
    NotSupported notSupported = new NotSupported();

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = InvalidCommandException.class)
    public void invokeNotSupported_expectInvalidCommandException() throws Exception {
        Exit exit = new Exit();
        exit.setNext(notSupported);

        exit.validateAndExecuteCommand("Not supported", new Monitor());
        Mockito.verify(notSupported).validateAndExecuteCommand("Not supported", Mockito.any(Monitor.class));
    }
}
