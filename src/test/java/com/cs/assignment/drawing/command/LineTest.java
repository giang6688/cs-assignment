package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.constant.PixelState;
import com.cs.assignment.drawing.exception.BeyondBoundaryException;
import com.cs.assignment.drawing.exception.InvalidCommandException;
import com.cs.assignment.drawing.model.Monitor;
import com.cs.assignment.drawing.model.Pixel;
import com.cs.assignment.drawing.model.Screen;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.mockito.Mockito.verify;
import static org.junit.Assert.*;

public class LineTest {
    private Monitor monitor = new Monitor();

    @Mock
    private NotSupported notSupportedCommand;

    @Before
    public void setup() {
        monitor.setScreen(new Screen(20, 4));
        MockitoAnnotations.initMocks(this);
    }

    @Test(expected = InvalidCommandException.class)
    public void drawInvalidLine_expectInvalidCommandException() throws Exception {
        Line line = new Line();
        line.validateAndExecuteCommand("L 1 2 3 4", monitor);
    }

    @Test(expected = BeyondBoundaryException.class)
    public void drawInvalidLine_expectBeyondBoundaryException() throws Exception {
        Line line = new Line();
        line.validateAndExecuteCommand("L 20 5 20 6", monitor);
    }

    @Test
    public void drawValidLine() throws Exception {
        Line line = new Line();
        line.validateAndExecuteCommand("L 10 3 12 3", monitor);
        Screen screen = monitor.getScreen();
        screen.refresh(true);

        List<Pixel> pixels = screen.getListOfPixelsTurnedOn();

        assertEquals(3, pixels.size());
        assertEquals(PixelState.ENABLED, screen.getByCoordinate(10, 3).getValue());
        assertEquals(PixelState.ENABLED, screen.getByCoordinate(11, 3).getValue());
        assertEquals(PixelState.ENABLED, screen.getByCoordinate(12, 3).getValue());
    }

    @Test()
    public void expectChainProcessing() throws Exception {
        String cmd = "Z 20 5 20 6";

        Line line = new Line();
        line.setNext(notSupportedCommand);
        line.validateAndExecuteCommand(cmd, monitor);

        // As command not supported by "Line", thus it pass same command to next command processor in chain
        verify(notSupportedCommand).validateAndExecuteCommand(cmd, monitor);
    }
}
