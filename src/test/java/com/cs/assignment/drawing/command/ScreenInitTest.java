package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.constant.PixelState;
import com.cs.assignment.drawing.exception.BeyondBoundaryException;
import com.cs.assignment.drawing.model.Monitor;
import com.cs.assignment.drawing.model.Pixel;
import com.cs.assignment.drawing.model.Screen;
import org.junit.Before;
import org.junit.Test;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class ScreenInitTest {
    private Monitor monitor = new Monitor();

    @Mock
    private Line nextCommand;

    @Before
    public void setup() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void requestCreateScreen_success() throws Exception {
        ScreenInit comand = new ScreenInit();
        comand.validateAndExecuteCommand("C 20 4", monitor);

        assertNotNull(monitor.getScreen());
    }

    @Test(expected = BeyondBoundaryException.class)
    public void drawLineBeforeScreenExist_expectInvalidCommandException() throws Exception {
        ScreenInit comand = new ScreenInit();
        comand.validateAndExecuteCommand("C 20 4", monitor);
        assertNotNull(monitor.getScreen());

        Line line = new Line();
        comand.setNext(line);
        comand.validateAndExecuteCommand("L 20 5 20 6", monitor);
    }

    @Test
    public void drawLineAfterScreenCreated_expectChainProcessing() throws Exception {
        ScreenInit comand = new ScreenInit();
        comand.setNext(nextCommand);

        comand.validateAndExecuteCommand("C 20 4", monitor);
        assertNotNull(monitor.getScreen());

        comand.validateAndExecuteCommand("L 1 2 1 3", monitor);

        Mockito.verify(nextCommand).validateAndExecuteCommand("L 1 2 1 3", monitor);
    }

    @Test
    public void drawLineAfterScreenCreated_success() throws Exception {
        ScreenInit comand = new ScreenInit();
        comand.validateAndExecuteCommand("C 20 4", monitor);
        assertNotNull(monitor.getScreen());

        Line line = new Line();
        comand.setNext(line);
        comand.validateAndExecuteCommand("L 10 1 10 3", monitor);

        Screen screen = monitor.getScreen();
        screen.refresh(true);

        List<Pixel> pixels = screen.getListOfPixelsTurnedOn();

        assertEquals(3, pixels.size());
        assertEquals(PixelState.ENABLED, screen.getByCoordinate(10, 1).getValue());
        assertEquals(PixelState.ENABLED, screen.getByCoordinate(10, 2).getValue());
        assertEquals(PixelState.ENABLED, screen.getByCoordinate(10, 3).getValue());
    }

}
