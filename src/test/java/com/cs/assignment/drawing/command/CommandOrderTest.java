package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.model.Monitor;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class CommandOrderTest {

    @Test
    public void expectOrderedCommands() {
        Monitor monitor = new Monitor();
        monitor.init();

        List<Command> commands = monitor.getSupportedCommands();

        Assert.assertTrue(commands.get(0) instanceof Exit);
        Assert.assertTrue(commands.get(1) instanceof ScreenInit);

        Assert.assertTrue(commands.get(commands.size() - 2) instanceof Line);
        Assert.assertTrue(commands.get(commands.size() - 1) instanceof NotSupported);
    }
}
