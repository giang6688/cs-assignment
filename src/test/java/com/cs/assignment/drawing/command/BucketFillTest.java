package com.cs.assignment.drawing.command;

import com.cs.assignment.drawing.constant.PixelState;
import com.cs.assignment.drawing.model.Monitor;
import com.cs.assignment.drawing.model.Pixel;
import com.cs.assignment.drawing.model.Screen;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.util.List;

import static org.junit.Assert.*;

public class BucketFillTest {
    private Monitor monitor = new Monitor();

    @Before
    public void setup() throws Exception {
        monitor.init();
        MockitoAnnotations.initMocks(this);
        monitor.validateAndExecuteInstruction("C 20 4");
    }

    @Test
    public void fillColor_expectSuccess() throws Exception {
        monitor.validateAndExecuteInstruction("L 1 2 3 2");
        monitor.validateAndExecuteInstruction("R 14 1 16 3");
        monitor.validateAndExecuteInstruction("B 10 3 o");

        Screen screen = monitor.getScreen();
        monitor.getScreen().refresh(true);

        List<Pixel> turnedOn = screen.getListOfPixelsTurnedOn();
        assertEquals(11, turnedOn.size());

        // Test line
        assertTrue(turnedOn.contains(new Pixel(1, 2).on()));
        assertTrue(turnedOn.contains(new Pixel(2, 2).on()));
        assertTrue(turnedOn.contains(new Pixel(3, 2).on()));

        // Test rectangle
        assertTrue(turnedOn.contains(new Pixel(14, 1).on()));
        assertTrue(turnedOn.contains(new Pixel(14, 2).on()));
        assertTrue(turnedOn.contains(new Pixel(14, 3).on()));
        assertTrue(turnedOn.contains(new Pixel(15, 1).on()));
        assertFalse(turnedOn.contains(new Pixel(15, 2).on()));
        assertTrue(turnedOn.contains(new Pixel(15, 3).on()));
        assertTrue(turnedOn.contains(new Pixel(16, 1).on()));
        assertTrue(turnedOn.contains(new Pixel(16, 2).on()));
        assertTrue(turnedOn.contains(new Pixel(16, 3).on()));

        // Test fill color
        List<Pixel> coloreds = screen.getListOfPixelsColored();
        assertEquals(68, coloreds.size());
        assertFalse(coloreds.contains(new Pixel(16, 3).on()));
        assertTrue(coloreds.contains(new Pixel(1, 1).fill(PixelState.DEFAULT_COLOR)));
        assertTrue(coloreds.contains(new Pixel(20, 4).fill(PixelState.DEFAULT_COLOR)));
    }

    @Test
    public void fillColor1_expectSuccess() throws Exception {
        monitor.validateAndExecuteInstruction("R 14 1 17 3");
        monitor.validateAndExecuteInstruction("B 15 2 o");

        Screen screen = monitor.getScreen();
        monitor.getScreen().refresh(true);

        // Test fill color
        List<Pixel> coloreds = screen.getListOfPixelsColored();
        assertEquals(2, coloreds.size());
        assertTrue(coloreds.contains(new Pixel(15, 2).fill(PixelState.DEFAULT_COLOR)));
        assertTrue(coloreds.contains(new Pixel(16, 2).fill(PixelState.DEFAULT_COLOR)));
    }

}
