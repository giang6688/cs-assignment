package com.cs.assignment.drawing.command;

public class SlopeTest {
    /*
    https://www.youtube.com/watch?v=IDFB5CDpLDE
    https://www.geeksforgeeks.org/bresenhams-line-generation-algorithm/
    https://classic.csunplugged.org/wp-content/uploads/2014/12/Lines.pdf
     */
    public static void main(String[] args) {
        int x1 = 4, y1 = 11, x2 = 8, y2 = 17;
        bresenham1(x1, y1, x2, y2);
    }

    static void bresenham(int x1, int y1, int x2, int y2) {
        int m_new = 2 * (x2 - x1);
        int slope_error_new = m_new - (y2 - y1);

        for (int x = x1, y = y1; x <= x2; x++) {
            System.out.print("(" + x + "," + y + ")\n");

            // Add slope to increment angle formed
            slope_error_new += m_new;

            // Slope error reached limit, time to
            // increment y and update slope error.
            if (slope_error_new >= 0) {
                y++;
                slope_error_new -= 2 * (x2 - x1);
            }
        }
    }

    static void bresenham1(int x1, int y1, int x2, int y2) {
        int rise = y2 - y1;
        int run  = x2 - x1;

        // vertical line
        if (run == 0) {
            for (int i = y1; i < y2 + 1; i++) {
                System.out.println("(" + x1 + "," + i + ")\n");
            }
        } else {
            float slope = (float) rise / run;
            int adjust = slope >=0 ? 1 : -1;
            float offset = 0f;
            float threshold = 0.5f;

            if (slope <= 1 && slope >= -1) {
                int y = y1;
                float delta = Math.abs(slope);

                if (x2 < x1) {
                    //TODO implement this
                }

                for (int x = x1; x < x2 + 1; x++) {
                    System.out.println("1.(" + x + "," + y + ")");

                    offset += delta;

                    if (offset >= threshold) {
                        y += adjust;
                        threshold += 1;
                    }
                }
            } else {
                float delta = Math.abs((float) run / rise);
                int x = x1;
                if (y2 < y1) {
                    //TODO implement this
                }
                for (int y = y1; y < y2 + 1; y++) {
                    System.out.println("2.(" + x + "," + y + ")");

                    offset += delta;

                    if (offset >= threshold) {
                        x += adjust;
                        threshold += 1;
                    }
                    System.out.println(delta + ":" + offset + ":" + threshold);
                }
            }
        }
    }

}
