package com.cs.assignment.drawing.model;

import com.cs.assignment.drawing.constant.PixelState;
import com.cs.assignment.drawing.exception.BeyondBoundaryException;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

public class ScreenTest {

    Screen screen;

    @Before
    public void setup() {
        screen = new Screen(20, 4);

        screen.getPixels().get(2).setValue(PixelState.DEFAULT_COLOR);

        screen.getPixels().get(21).setValue(PixelState.ENABLED);
    }

    @Test
    public void get_ListOfPixelsTurnedOn_expectedOne() {
        Assert.assertEquals(1, screen.getListOfPixelsTurnedOn().size());
    }

    @Test
    public void get_ListOfPixelsColored_expectedOne() {
        Assert.assertEquals(1, screen.getListOfPixelsTurnedOn().size());
        Assert.assertEquals(PixelState.DEFAULT_COLOR, screen.getListOfPixelsColored().get(0).getValue());
    }

    @Test(expected = BeyondBoundaryException.class)
    public void get_ByCoordinate_exceptionThrown() throws BeyondBoundaryException {
        screen.getByCoordinate(21, 2);
    }

    @Test
    public void get_ByCoordinate_expectEnabledPixel() throws BeyondBoundaryException {
        Assert.assertEquals(PixelState.ENABLED, screen.getByCoordinate(2, 2).getValue());
    }

    @Test
    public void get_ByCoordinate_expectPixel() throws BeyondBoundaryException {
        Assert.assertEquals(new Pixel(1,2), screen.getByCoordinate(1, 2));
        Assert.assertEquals(new Pixel(1,1), screen.getByCoordinate(1, 1));
        Assert.assertEquals(new Pixel(20,1), screen.getByCoordinate(20, 1));
        Assert.assertEquals(new Pixel(20,4), screen.getByCoordinate(20, 4));
    }
}
