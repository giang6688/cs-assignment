package com.cs.assignment.drawing.model;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

public class MonitorTest {

    private Monitor monitor;

    @Before
    public void init() {
        monitor = new Monitor();
        monitor.init();

    }

    @Test
    public void innitProcessingChain_success() {
        Assert.assertEquals(6, monitor.getSupportedCommands().size());
    }

    @Test
    public void drawScreen_success() throws Exception {
        assertNull(monitor.getScreen());

        monitor.validateAndExecuteInstruction("C 10 2");
        assertNotNull(monitor.getScreen());

    }


}
